import Vue from 'vue'
import App from './App.vue'
import http from 'axios'
import vuetify from './plugins/vuetify'
import router from './router/router'
import { loadFonts } from './plugins/webfontloader'
import Vuelidate from 'vuelidate'

loadFonts()

new Vue({
  render: (h) => h(App),
  vuetify,
  router,
  Vuelidate
}).$mount('#app');

// http.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'

// borramos el localStorage si obtenemos un error de autenticación
http.interceptors.response.use(
  undefined, err => {
    if (err.response.status === 401 || err.response.status === 403)
      localStorage.removeItem("token")
    return Promise.reject(err)
  }
)
import Vue from 'vue'
import Router from 'vue-router'

import hooks from './hooks'

import Home from '../components/Home.vue'
import PareaLogin from '../components/login/PareaLogin.vue'
import PareaRegister from '../components/login/PareaRegister.vue'
import PareaDashboard from '../components/user/PareaDashboard.vue'
import PareaAmigos from '../components/user/PareaAmigos.vue'
import PareaMensajes from '../components/mensajes/PareaMensajes.vue'
import HelloWorld from '../components/HelloWorld.vue'
import NotFound from '../components/NotFound.vue'
import PareaJitsi from '../components/quedadas/PareaJitsi.vue'

Vue.use(Router)

const
    routes = [
        {
            path: '/',
            name: 'home',
            component: HelloWorld
        },
        {
            path: '/login',
            name: 'login',
            component: PareaLogin,
        },
        {
            path: '/register',
            name: 'register',
            component: PareaRegister,
        },
        {
            path: '/dashboard',
            name: 'parea-dashboard',
            component: PareaDashboard,
            props: true,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/friends',
            name: 'parea-amigos',
            component: PareaAmigos,
            props: true,
            meta: {
                requireAuth: true
            },
        }, {
            path: '/messages',
            name: 'parea-mensajes',
            component: PareaMensajes,
            props: true,
            meta: {
                requireAuth: true
            },
        }, {
            path: '/meets/:id',
            name: 'parea-jitsi',
            component: PareaJitsi,
            props: ['quedada'],
            meta: {
                requireAuth: true
            },
        },
        {
            path: '/404',
            name: '404',
            component: NotFound
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]

const router = new Router({
    mode: 'history',
    routes
})

hooks(router)

export default router
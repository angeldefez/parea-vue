import axios from 'axios'

export default router => {
    // comprueba si el usuario está registrado o se autentifica si es necesario
    router.beforeEach((to, from, next) => {
        const REQUIRE_AUTH = to.matched.some(record => record.meta.requireAuth)

        if (REQUIRE_AUTH) {
            if (!localStorage.getItem('token')) {
                next({
                    path: '/login'
                })
            } else {
                axios.defaults.headers.common['Authorization'] = 'JWT ' + localStorage.getItem('token').split(' ')[1]
                next()
            }
        } else
            axios.defaults.headers.common['Authorization'] = null

        return next()
    })
}
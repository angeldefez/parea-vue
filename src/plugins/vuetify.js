import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'
// import '@mdi/font/css/materialdesignicons.css'
// import 'vuetify/styles'
require('../styles/overrides.sass')

Vue.use(Vuetify)

const opts = {
  theme: {
    dark: false,
    themes: {
      light: {
        primary: '#344955',
        secondary: '#F9AA33',
        tertiary: '#232F34',
        quaternary: '#4A6572',
      },
    },
    icons: {
      iconfont: 'mdi', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
    },
  },
}

export default new Vuetify(opts)
// const Vuetify = require('./src/plugins/vuetify.js')

import { defineConfig } from 'vite'
import { createVuePlugin } from 'vite-plugin-vue2'
import { VuetifyResolver } from 'unplugin-vue-components/resolvers';
import Components from 'unplugin-vue-components/vite';
import { viteCommonjs, esbuildCommonjs } from '@originjs/vite-plugin-commonjs'
import babel from 'vite-plugin-babel'

const path = require('path')

export default defineConfig({
  plugins: [
    // Vuetify,
    createVuePlugin({
      autoImport: true,
      compilerOptions: { whitespace: 'condense' }
    }),
    babel,
    viteCommonjs(),
    Components({
      resolvers: [
        // Vuetify
        VuetifyResolver(),
      ],
    }),
  ],
  optimizeDeps: {
    esbuildOptions: {
      plugins: [
        // Solves:
        // https://github.com/vitejs/vite/issues/5308
        // esbuildCommonjs(['vue-cli-plugin-vuetify-preset-reply'])
        esbuildCommonjs()
      ],
    },
  },
  server: {
    port: 3000,
    host: "0.0.0.0",
    https: false,
    hmr: {
      clientPort: 80,
      // overlay: false
    },
    watch: {
      usePolling: true,
      // alwaysStat: true,
      // useFsEvents: false,
      ignored: ['!**/node_modules/**']
    }
  },
  define: { 'process.env': {} },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
      '~vuetify': path.resolve(__dirname, '.'),
      // this is required for the SCSS modules
      find: /^~(.*)$/,
      replacement: '$1',
    },
  },
})